import { LightningElement, track, api } from 'lwc';
import getFiles from '@salesforce/apex/Cuiab_MoveFilesController.getFiles';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';
import moveFiles from '@salesforce/apex/Cuiab_MoveFilesController.moveFiles';
import getCaseId from '@salesforce/apex/Cuiab_MoveFilesController.getCaseId';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { NavigationMixin } from 'lightning/navigation';
import updateVerificationStatus from '@salesforce/apex/Cuiab_MoveFilesController.updateVerificationStatus';
import { refreshApex } from '@salesforce/apex';
import { CloseActionScreenEvent } from 'lightning/actions';

export default class Cuiab_movefiles_task_to_case extends NavigationMixin(LightningElement) {
    @track filesInSource = [];
    @track verificationModal = false;
    @track verificationId;
    @api recordId;
    @track caseId;
    @track filesToMove = [];
    loaddata() {
        getCaseId({
            recordid: this.recordId

        }).then((response) => {
            console.log(response);
            this.caseId = response;
            getFiles({
                source: this.recordId,
                newSource: this.caseId
            })
                .then((response) => {

                    if (response.length == 0) {
                        this.showToast('No Doc', response)
                    } else {
                        //console.log('$$$$'+response);
                        console.log(response.length);
                        this.filesInSource = response;
                        this.filesInSource.forEach(element => {
                            element.checked = false;
                        });
                        this.filesInSource.forEach(element => {
                            if (element.Status__c == 'Verified') {
                                element.isVerified = true;
                            }
                        });

                    }
                })


        })
    }
    connectedCallback() {
        console.log(this.recordId);
        this.loaddata();
    }

    showToast(title, response) {
        if (title == 'No Doc') {
            const evt = new ShowToastEvent({
                title: "No document available",
                message: response,
                variant: "error"
            });
            this.dispatchEvent(evt);
        }
        else if (title == 'No File') {
            const evt = new ShowToastEvent({
                title: "No Verified File is Selected to Move",
                message: response,
                variant: "error"
            });
            this.dispatchEvent(evt);

        }
    }


    handlepreviewFileAction(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state: {
                selectedRecordId: event.currentTarget.dataset.cdid
            }
        });


    }
    handleVerifyButtonAction(event) {
        this.verificationModal = true;
        this.verificationId = event.currentTarget.dataset.cdid2;
        console.log(this.verificationId);
    }
    handleVerify(event) {
        console.log('verificationId');
        updateVerificationStatus({
            contentDocumentId: this.verificationId
        }).then(result => {
            console.log(result);
            refreshApex(this.filesInSource);

        })
            .catch(error => {
                // TODO Error handling
                console.log(error);
            });
        this.verificationModal = false;
        this.loaddata();

    }
    handleDontVerify(event) {
        this.verificationModal = false;
    }
    handleSelectedCheckbox(event) {
        this.filesInSource[event.currentTarget.dataset.index].checked = event.target.checked;
    }
    handleMoveSelectedFiles(event) {
        console.log(this.filesInSource);
        console.log(this.caseId);
        this.loaddata();
        this.filesInSource.forEach(element => {
            if (element.checked == true && element.Status__c == 'Verified') {
                console.log(element.checked + '' + element.ContentDocumentId);
                this.filesToMove.push(element.ContentDocumentId);
            }
        });
        console.log(this.filesToMove);
        moveFiles({
            source: this.recordId,
            newSource: this.caseId,
            filesToMove: this.filesToMove
        })
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                // TODO Error handling
                this.showToast('No File', error);
                console.log(error);
            });
    }
    HideModalPopup() {
        console.log('>>');
        this.dispatchEvent(new CloseActionScreenEvent());
    }

}