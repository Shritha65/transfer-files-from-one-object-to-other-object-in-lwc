public with sharing class Cuiab_MoveFilesController {
    @AuraEnabled
    public static Id getCaseId(Id recordid){
        try {
          List<Task> taskList=[ Select Id, WhatId From Task where Id=:recordid];
            System.debug('>>>>'+taskList[0].WhatId);
            return taskList[0].WhatId;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static List<ContentVersion> getFiles(Id source , Id newSource) 
    {
        try{
        Set<Id> contentDocIdsour= new Set<id>();
        Set<Id> contentDocIddes= new Set<id>();
        System.debug('***'+newSource);
        List<ContentVersion> res= new List<ContentVersion>();
        List<ContentDocumentLink> cntLink = [Select Id, ContentDocumentId, LinkedEntityId From ContentDocumentLink Where LinkedEntityId =: source];
        List<ContentDocumentLink> cntLinkdes = [Select Id, ContentDocumentId, LinkedEntityId From ContentDocumentLink Where LinkedEntityId =: newSource];
        for(ContentDocumentLink cdl:cntLinkdes)
        {
            contentDocIddes.add(cdl.ContentDocumentId);
        }   
        for(ContentDocumentLink cdl:cntLink)
        {
            if(!contentDocIddes.contains(cdl.ContentDocumentId)){
                    contentDocIdsour.add(cdl.ContentDocumentId);
           }
        }
        System.debug('>>>'+contentDocIdsour);
        res=  [SELECT Id, Title,Status__c, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :contentDocIdsour AND IsLatest=True];
        System.debug('>>>'+res);
        return res;
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

    }
    @AuraEnabled
    public static String  moveFiles(Id source , Id newSource ,List<Id>  filesToMove)
    {
        try{
        List<ContentDocumentLink> insertLinks= new List<ContentDocumentLink>();
        List<ContentDocumentLink> deleteLinks = new List<ContentDocumentLink>();

        Set<Id> documentIdPresent = new Set<Id>();
        System.debug('>>>'+filesToMove);
        for(ContentDocumentLink cdl : [
            Select Id, ContentDocumentId, IsDeleted, LinkedEntityId, ShareType, SystemModstamp, Visibility 
            From ContentDocumentLink 
            Where ContentDocumentId IN: filesToMove
        ]){
            if(!documentIdPresent.contains(cdl.ContentDocumentId)){
                ContentDocumentLink clonedcontdl = cdl.clone(false, true, false, false);
                clonedcontdl.LinkedEntityId = newSource;
                insertLinks.add(clonedcontdl);
                deleteLinks.add(cdl);
                documentIdPresent.add(cdl.ContentDocumentId);
            }
        }
        System.debug('***'+insertLinks);
        if(!insertLinks.isEmpty()){
            insert insertLinks;
        }
       return 'Success';
    }
  
    catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
    }
    }
    @AuraEnabled
    public static String  updateVerificationStatus(Id contentDocumentId)
    {
        try{
            List<ContentVersion> contVersionList= [SELECT Id, Title,Status__c, IsMajorVersion,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId = :contentDocumentId AND IsLatest=True];
            System.debug('&&&'+contVersionList[0].IsMajorVersion);
            contVersionList[0].Status__c='Verified';
            update contVersionList;
            return 'Success';
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

    }

}
